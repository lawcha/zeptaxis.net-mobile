package com.example.laurent.zeptaxisnet;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import org.w3c.dom.Document;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XMLGetter {

    static {
        HttpsTrustManager.allowAllSSL();
    }
    public static HashMap<String, String>[] getAnimes(String url, int nbCategories) {
        HashMap<String, String>[] arr = new HashMap[nbCategories];
        Document xml = initConnection(url);
        for (int i = 0; i < nbCategories; i++) {
            NodeList animes = xml.getElementById("letters" + i).getElementsByTagName("li");
            HashMap<String, String> list = new HashMap<>();
            for (int j = 0; j < animes.getLength(); j++) {
                Element elem = (Element)animes.item(j);
                list.put(elem.getAttribute("data-name"), url + elem.getAttribute("data-href"));
            }
            arr[i] = list;
        }
        return arr;
    }

    public static ArrayList<String> getLetters(String url) {
        ArrayList<String> letters = new ArrayList<>();
        Document xml = initConnection(url);
        NodeList letterNodes = xml.getElementById("lettersHeader").getElementsByTagName("a");
        for (int i = 0; i < letterNodes.getLength(); i++) {
            letters.add(letterNodes.item(i).getTextContent());
        }
        return letters;
    }

    public static HashMap<String, HashMap<String, String>> getEpisodes(String url) {
        HashMap<String, HashMap<String, String>> episodes = new HashMap<>();
        Document xml = initConnection(url);
        NodeList epNodes = xml.getElementById("directory-listing").getElementsByTagName("li");
        String firstName = ((Element)epNodes.item(1)).getAttribute("data-name");
        if(firstName.endsWith(".mp4") || firstName.endsWith(".mkv") || firstName.endsWith(".avi")){ //if one season only (->no dir)
            HashMap<String, String> season = new HashMap<>();
            for (int i = 1; i < epNodes.getLength(); i++) {
                String elemName = ((Element)epNodes.item(i)).getAttribute("data-name");
                String elemUrl = MainActivity.BASE_URL + "/" + ((Element)epNodes.item(i)).getAttribute("data-href");
                season.put(elemName, elemUrl);
            }
            episodes.put("Saison 1", season);
        } else {
            for (int i = 1; i < epNodes.getLength(); i++) {
                HashMap<String, String> season = new HashMap<>();
                String seasonName = ((Element)epNodes.item(i)).getAttribute("data-name");
                String seasonUrl = MainActivity.BASE_URL + "/" + ((Element)epNodes.item(i)).getAttribute("data-href");
                if(seasonName.endsWith(".mp4") || seasonName.endsWith(".mkv") || seasonName.endsWith(".avi")) { //if dir has directories AND FILES
                    season.put(seasonName, seasonUrl);
                    episodes.put("\uE83A Autres", season);
                    break;
                }
                xml = initConnection(seasonUrl);
                NodeList seepNodes = xml.getElementById("directory-listing").getElementsByTagName("li");
                for (int j = 1; j < seepNodes.getLength(); j++) {
                    String elemName = ((Element)seepNodes.item(j)).getAttribute("data-name");
                    String elemUrl = MainActivity.BASE_URL + "/" + ((Element)seepNodes.item(j)).getAttribute("data-href");
                    season.put(elemName, elemUrl);
                }
                episodes.put(seasonName, season);
            }
        }
        return episodes;
    }

    private static Document initConnection(String url) {
        try {
            URL myUrl = new URL(url);
            HttpsURLConnection conn = (HttpsURLConnection) myUrl.openConnection();
            conn.setRequestProperty("Authorization", getAuth(myUrl.getUserInfo()));
            InputStream is = conn.getInputStream();
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(is, "UTF-8");
            TagNode body = node.getElementsByName("body", true)[0];
            return new DomSerializer(cleaner.getProperties(), true).createDOM(body);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getAuth(String log) {
        return "Basic " + android.util.Base64.encodeToString(log.getBytes(), android.util.Base64.DEFAULT);
    }
}
