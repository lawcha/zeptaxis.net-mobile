package com.example.laurent.zeptaxisnet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class AnimesFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private final int sectionNumber;
    private View rootView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AnimesFragment newInstance(int sectionNumber) {
        AnimesFragment fragment = new AnimesFragment(sectionNumber);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public AnimesFragment(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        Log.d("SECTION_N", "" + sectionNumber);
        afterInitList();
        return rootView;
    }

    public void afterInitList() {
        ArrayList<String> list = new ArrayList<>();
        list.addAll(MainActivity.animesList[sectionNumber].keySet());
        Collections.sort(list);
        RecyclerView animeList = (RecyclerView) rootView.findViewById(R.id.anime_list);
        animeList.addItemDecoration(new DividerItemDecoration(animeList.getContext(), DividerItemDecoration.VERTICAL));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(list, this.getContext());
        animeList.setAdapter(adapter);
        animeList.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }
}