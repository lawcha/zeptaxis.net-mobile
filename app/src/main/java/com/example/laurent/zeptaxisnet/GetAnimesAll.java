package com.example.laurent.zeptaxisnet;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.HashMap;

public class GetAnimesAll extends AsyncTask<String, Void, Wrapper> {

    private MainActivity main;

    public GetAnimesAll(MainActivity main) {
        this.main = main;
    }

    @Override
    protected Wrapper doInBackground(String... params) {
        Wrapper w = new Wrapper();
        w.letters = XMLGetter.getLetters(params[0]);
        w.animes = XMLGetter.getAnimes(params[0], w.letters.size());
        return w;
    }

    protected void onPostExecute(Wrapper w) {
        main.afterInitTabs(w.letters);
        main.afterInitList(w.animes);
    }
}
class Wrapper {
    HashMap<String, String>[] animes;
    ArrayList<String> letters;
}
