package com.example.laurent.zeptaxisnet;

import android.os.AsyncTask;
import java.util.ArrayList;

public class GetLetters extends AsyncTask<String, Void, ArrayList<String>> {

    private MainActivity main;

    public GetLetters(MainActivity main) {
        this.main = main;
    }

    @Override
    protected ArrayList<String> doInBackground(String... urls) {
        return XMLGetter.getLetters(urls[0]);
    }

    protected void onPostExecute(ArrayList<String> letters) {
        main.afterInitTabs(letters);
    }
}
