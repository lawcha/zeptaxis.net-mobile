package com.example.laurent.zeptaxisnet;

import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
   
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    static int NB_PAGES = 1;
    static int currentTab = 0;
    static HashMap<String, String>[] animesList; //Map<AnimeName, URL>[]
    static ArrayList<String> lettersList;
    public static final String USERNAME = "robert";
    public static final String PASSWORD = "la_bisole";
    public static final String SAVE_BUNDLE_TABS = "animes_tabs";
    public static final String SAVE_BUNDLE_ANIMES = "animes_map";

    private ViewPager mViewPager; //The {@link ViewPager} that will host the section contents.
    private TabLayout tabLayout;
    public static final String BASE_URL = String.format("https://%s:%s@admin.zeptaxis.net/anime", USERNAME, PASSWORD);
    public static final String CHANGELOG_URL = BASE_URL + "/change.log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        new GetAnimesAll(this).execute(BASE_URL); //Getting home informations (tabs + animes list) from url

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTab = tab.getPosition();
                Log.d("PAGE_NUM", "" + currentTab);
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}
            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    public void afterInitTabs(ArrayList<String> letters) {
        for(String let: letters) {
            Log.d("LETTER_GETTED",let);
            tabLayout.addTab(tabLayout.newTab().setText(let));
        }
        NB_PAGES = letters.size();
        lettersList = letters;

        //new GetAnimes(this).execute(String.format("https://%s:%s@admin.zeptaxis.net/anime", MainActivity.USERNAME, MainActivity.PASSWORD));
    }

    public void afterInitList(HashMap<String, String>[] map) {
        animesList = map;
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    
  

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return AnimesFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return NB_PAGES;
        }
    }


}
