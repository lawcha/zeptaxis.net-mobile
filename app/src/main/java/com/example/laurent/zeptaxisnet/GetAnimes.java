package com.example.laurent.zeptaxisnet;

import android.os.AsyncTask;
import java.util.HashMap;

public class GetAnimes extends AsyncTask<String, Void, HashMap<String, String>[]> {

    private MainActivity main;

    public GetAnimes(MainActivity main) {
        this.main = main;
    }

    @Override
    protected HashMap<String, String>[] doInBackground(String... params) {
        return XMLGetter.getAnimes(params[0], MainActivity.NB_PAGES);
    }

    protected void onPostExecute(HashMap<String, String>[] map) {

        main.afterInitList(map);
    }
}
