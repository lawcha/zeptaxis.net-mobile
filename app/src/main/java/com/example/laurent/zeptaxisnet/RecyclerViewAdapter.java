package com.example.laurent.zeptaxisnet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    public static final String TAG = "RecyclerViewAdapter";
    private ArrayList<String> mAnimeTitles = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapter(ArrayList<String> animeTitles, Context context) {
        this.mAnimeTitles = animeTitles;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        Log.d(TAG, "onBindViewHolder: called.");
        viewHolder.item.setText(mAnimeTitles.get(i));
        viewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = MainActivity.animesList[MainActivity.currentTab].get(mAnimeTitles.get(i));
                Intent intent = new Intent(mContext, EpisodesActivity.class);
                intent.putExtra("title", mAnimeTitles.get(i));
                intent.putExtra("url", url);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAnimeTitles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView item;
        RelativeLayout parent;

        public ViewHolder(View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.anime_list_item);
            parent = itemView.findViewById(R.id.parent_layout);
        }
    }
}
