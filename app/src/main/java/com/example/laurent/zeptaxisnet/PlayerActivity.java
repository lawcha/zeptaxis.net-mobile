package com.example.laurent.zeptaxisnet;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import java.util.HashMap;

public class PlayerActivity extends Activity {

    private CustomVideoView videoView;
    private int stopTime = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        String url = getIntent().getStringExtra("url");

        videoView = (CustomVideoView) findViewById(R.id.videoView);
        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {
                //Play
            }

            @Override
            public void onPause() {
                //Pause
            }
        });
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", getAuth(MainActivity.USERNAME + ":" + MainActivity.PASSWORD));
        Uri uri = Uri.parse(url);
        videoView.setVideoURI(uri, header);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.start();
    }

    @Override
    protected void onPause() {
        stopTime = videoView.getCurrentPosition();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if(stopTime != -1) {
            videoView.seekTo(stopTime);
            videoView.start();
            stopTime = -1;
        }
        super.onResume();
    }

    public static String getAuth(String log) {
        return "Basic " + android.util.Base64.encodeToString(log.getBytes(), android.util.Base64.DEFAULT);
    }
}
class CustomVideoView extends VideoView {

    private PlayPauseListener mListener;

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public void setPlayPauseListener(PlayPauseListener listener) {
        mListener = listener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.onPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.onPlay();
        }
    }

    public static interface PlayPauseListener {
        void onPlay();
        void onPause();
    }
}