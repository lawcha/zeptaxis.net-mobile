package com.example.laurent.zeptaxisnet;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class EpisodeRecyclerViewAdapter extends RecyclerView.Adapter<EpisodeRecyclerViewAdapter.ViewHolder> {
    public static final String TAG = "RecyclerViewAdapter";
    private ArrayList<String> mEpisodes;
    private Context mContext;
    private String mSeasonName;

    public EpisodeRecyclerViewAdapter(ArrayList<String> episodes, Context context, String seasonName) {
        this.mEpisodes = episodes;
        this.mContext = context;
        this.mSeasonName = seasonName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        Log.d(TAG, "onBindViewHolder: called.");
        viewHolder.item.setText(mEpisodes.get(i));
        viewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = EpisodesActivity.episodes.get(mSeasonName).get(mEpisodes.get(i));

                /*Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(url), "video/*");
                mContext.startActivity(Intent.createChooser(intent, "Complete action using"));*/

                Intent intent = new Intent(mContext, PlayerActivity.class);
                intent.putExtra("url", url);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEpisodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView item;
        RelativeLayout parent;

        public ViewHolder(View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.anime_list_item);
            parent = itemView.findViewById(R.id.parent_layout);
        }
    }
}
