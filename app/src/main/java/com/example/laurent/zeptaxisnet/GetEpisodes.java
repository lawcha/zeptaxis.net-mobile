package com.example.laurent.zeptaxisnet;

import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.HashMap;

public class GetEpisodes extends AsyncTask<String, Void, HashMap<String, HashMap<String, String>>> {

    private EpisodesActivity main;

    public GetEpisodes(EpisodesActivity main) {
        this.main = main;
    }

    @Override
    protected HashMap<String, HashMap<String, String>> doInBackground(String... urls) {
        return XMLGetter.getEpisodes(urls[0]);
    }

    protected void onPostExecute(HashMap<String, HashMap<String, String>> episodes) {
        main.afterGettingEpisodes(episodes);
    }
}